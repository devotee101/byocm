variable "access_key" {
  description = "INPUT your AWS access_key"
}

variable "secret_key" {
  description = "INPUT your AWS secret_key"
}

variable "region" {
  description = "AWS region to host your network"
  default     = "eu-west-1"
}

variable "vpc_cidr" {
  description = "CIDR for VPC"
  default     = "10.128.0.0/16"
}

variable "public_subnet_cidr" {
  description = "CIDR for public subnet"
  default     = "10.128.0.0/24"
}

variable "private_subnet_cidr" {
  description = "CIDR for private subnet"
  default     = "10.128.1.0/24"
}

variable "amis" {
  description = "Base AMI to launch the instances with"
  default = {
    eu-west-1 = "ami-db1688a2" 
  }
}
