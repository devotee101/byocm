output "docker.ip" {
  value = "${aws_instance.docker.public_ip}"
}
