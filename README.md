byocm
=====
Bring your own configuration management challenge.

## Overview
The solution is split into two groups of containers - monitoring and wpapp, each in its own subfolder to allow for separate deployment.

## Prerequisites
* Terraform >= 0.11.3
* Docker >= 17.12.0-ce
* Docker Compose >= 1.18.0

## Assumptions
Suitable terminal, either native Mac, Linux or GitBash on Windows.

## Installation
Clone this repository and cd into the byocm folder

```bash
git clone https://bitbucket.org/vividcloud/byocm
cd byocm
```

## Running the containers in dev
**PLEASE NOTE:** It's strongly suggested to set environment variables prior to running the `docker-compose`, however defaults will be applied if none have been specified.

Also, if you have not created the monitoring containers before running the WordPress Tier, you'll get an errror because there's no external network. To fix, run:
```
docker network create monitoring_byocm
```

### Monitoring Containers
The environment variables are as follows:
| Variable | Purpose |
|----------|---------|
| ADMIN_USER | Grafana admin user |
| ADMIN_PASSWORD | Grafana admin password |

To run the monitoring container group:
```bash
cd ./monitoring
ADMIN_USER=admin ADMIN_PASSWORD=admin docker-compose up -d
```

### Wordpress Containers
The environment variables are as follows:
| Variable | Purpose |
|----------|---------|
| MYSQL_ROOT_PASSWORD | MYSQL root password |
| MYSQL_USER | MYSQL WordPress DB user |
| MYSQL_PASSWORD | MYSQL WordPress DB password |

To run the WordPress container group:
```bash
cd ./wpapp
MYSQL_ROOT_PASSWORD=<mysql root password> MYSQL_USER=<mysql user> MYSQL_PASSWORD=<mysql password> docker-compose up -d
```

## Deploying the containers to EC2
The following instructions assume you've changed directory to the ./infrastructure folder.

### Variables
Create a terraform.tfvars file using variables in the terraform.tfvars.example file and set the variables appropriately.

### SSH Keys
Generate ssh keys as follows:
`ssh-keygen -t rsa -C "insecure-deployer" -P '' -f ssh/insecure-deployer`

Run `terraform init` to download the necessary providers.

Run `terraform plan` to check the execution plan matches what you expect.

Run `terraform apply` to build the infrastructure. Once completed, the IP address of the new EC2 instance will be displayed.

To connect, run `ssh -i ssh/insecure-deployer ec2-user@$(terraform output docker.ip)`

## TODO:
* Remove hard-coded admin username and password from the monitoring stack
* Remove git pull in cloud-init and replace with file provider (tried but couldn't get it working reliably)
* Add insightful dashboards for bonus points
* Add shell scripts to simplify setup (ssh keygen, ssh connect, etc)