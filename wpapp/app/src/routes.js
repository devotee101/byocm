const companies = require('./lib/companies')

const router = app => {
  app.get('/companies', (req, res) => res.status(200).send(companies))
}

module.exports = router
