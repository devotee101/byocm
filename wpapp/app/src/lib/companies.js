const faker = require('faker')
const COUNT = 25
var companies = []

for (var i = 0; i <= COUNT - 1; i++) {
  companies.push({
    companyName: faker.company.companyName()
  })
}

module.exports = Object.freeze(companies)
